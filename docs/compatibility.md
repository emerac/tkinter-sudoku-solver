# Compatibility

## Python

| Version | Compatibility | Tested      |
| ------- | ------------- | ----------- |
| 3.9     | Full          | Yes         |
| 3.8     | Unknown       | Partially\* |
| 3.7     | Unknown       | Partially\* |

\*Testing was performed using a Docker container, which cannot display
the GUI. Due to the nature of the code, the GUI is necessary to be able
to run certain tests. Therefore, true compatibility is unknown.

## Operating systems

| OS      | Compatibility | Tested | Notes                                    |
| ------- | ------------- | ------ | ---------------------------------------- |
| Linux   | Full          | Yes    | None                                     |
| Windows | Unknown       | No     | No known incompatible features are used  |
| macOS   | Unknown       | No     | No known incompatible features are used  |
