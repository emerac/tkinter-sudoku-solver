# Documentation

## Compatibility

For compatibility information regarding Python versions and operating
systems, view the [compatibility](compatibility.md) page.

## Documentation

For instructions on how to view the documentation for source modules,
view the [documenation](documentation.md) page.

## Installation

For those who would like to develop this project, view the
[installation](installation.md) page for instructions on setting up
an environment.

## Testing

If you would like to run the available tests, view the
[testing](testing.md) page.
