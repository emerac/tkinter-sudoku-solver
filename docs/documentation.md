# View the documentation

Documentation for this project is generated using pydoc.

To view the documentation, clone this repository and then run
`python -m pydoc {module_name}` for the module whose documentation you
would like to view. The source modules are stored in
[tssolver](../tssolver).

For example:

`python -m pydoc tssolver/interface.py` displays the documentation for
the interface module in the terminal.

`python -m pydoc -w tssolver/interface.py` creates an html version of
the documentation in the current directory that can be viewed in a
browser.
