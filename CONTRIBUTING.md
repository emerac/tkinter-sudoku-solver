# How to contribute

## Reporting bugs

Bug reports are very welcome. Please report bugs via the
[GitLab issue tracker](https://gitlab.com/emerac/tkinter-sudoku-solver/-/issues).

Reports should contain:

1. The expected behavior
2. A brief explanation of the bug
3. The steps necessary to reproduce the bug

## Writing code

This application is considered "feature-complete" in regards to the
features that were intended for it. However, contributions are still
welcome and the [documentation](docs/README.md) contains information
that should help you get up and running.

### Pull requests

To facilitate the pull request process, please follow these guidelines:

* Make smaller, more focused changes.
* Test your code against the preexisting tests.
* If the current tests do not cover your code, please add new ones
that do.

---
**Thank you!**
