# Tkinter Sudoku Solver

## About

This application solves classic sudoku puzzles. It is written in
Python with an interface built using tkinter.

Some notable features about this application:

* Real-time visual representation of the solution algorithm
* Solve user-defined puzzles or use built-in sample puzzles
* Control over the speed of the solver
* Colorblind mode

![regular mode](https://gitlab.com/emerac/tkinter-sudoku-solver/-/raw/master/docs/images/reg-mode.png)
![colorblind mode](https://gitlab.com/emerac/tkinter-sudoku-solver/-/raw/master/docs/images/cb-mode.png)

## Compatibility

Full compatibility with Python 3.9 on Linux.

More details regarding other Python version and operating systems can
be found in the
[documentation](https://gitlab.com/emerac/tkinter-sudoku-solver/-/blob/master/docs/compatibility.md).

## Installation

Running `pip install tssolver` will use pip to install the package.
After installing, `tssolver` will run the application.

## Contributing

Please report any bugs to the
[GitLab issue tracker](https://gitlab.com/emerac/tkinter-sudoku-solver/-/issues).
See
[CONTRIBUTING](https://gitlab.com/emerac/tkinter-sudoku-solver/-/blob/master/CONTRIBUTING.md)
for more details.

## License

This program is free software and is licensed under the GNU General
Public License. For the full license text, view the
[LICENSE](https://gitlab.com/emerac/tkinter-sudoku-solver/-/blob/master/LICENSE)
file.

Copyright © 2021 emerac
